function newKanjiSubscribe(parent, args, context, info) {
    return context.prisma.$subscribe.kanji({ mutation_in: ['CREATED'] }).node()
  }
  
  const newKanji = {
    subscribe: newKanjiSubscribe,
    resolve: payload => {
      return payload
    },
  }
  
  module.exports = {
    newKanji
  }