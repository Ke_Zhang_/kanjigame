async function addkanji(parent, args, context, info) {
    const kanjiExists = await context.prisma.$exists.kanji({
        character: args.character,
    })
    if (kanjiExists) {
        throw new Error(`character already in database: ${args.character}`)
    }
    
    return context.prisma.createKanji({
        character: args.character,
        hiragana: args.hiragana,
    })
}

module.exports = {
    addkanji
}