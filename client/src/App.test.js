import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import configStore from './configStore';
import { Provider } from 'react-redux';
import { BrowserRouter as Router, Route, Link } from "react-router-dom";

it('renders without crashing', () => {
  const div = document.createElement('div');
  ReactDOM.render(<Provider store={configStore()}><Router><App /></Router></Provider>, div);
  ReactDOM.unmountComponentAtNode(div);
});
