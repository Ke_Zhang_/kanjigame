import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Buttons from '../components/Buttons';
import Header from '../components/Header';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../App.css';
import { withRouter } from 'react-router-dom'
import { graphql, compose, Mutation } from 'react-apollo';
import {
    FETCH_QUERY,
} from './Query';
import {
    ADD_KANJI,
    DELETE_KANJI,
    updateKanjis
} from './Mutation';
import MaterialTable from 'material-table';
import gql from 'graphql-tag';

const NEW_KANJI_SUBSCRIPTION = gql`
  subscription {
    newKanji {
      id
      character
      hiragana
    }
  }
`

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 200
    },
});

class Dictionary extends Component {

    constructor(props) {
        super(props);
        this.state = {
            columns: [
                { title: 'Character', field: 'character' },
                { title: 'Hiragana', field: 'hiragana',  },
              ],
              data: [

              ],
        }
    }

    _subscribeToNewKanji = subscribeToMore => {
        subscribeToMore({
          document: NEW_KANJI_SUBSCRIPTION,
          updateQuery: (prev, { subscriptionData }) => {
            if (!subscriptionData.data) return prev
            const newKanji = subscriptionData.data.newKanji
            const exists = prev.kanjis.find(({ id }) => id === newKanji.id);
            if (exists) return prev;
      
            return Object.assign({}, prev, {
                kanjis: [...prev.kanjis, newKanji],
                __typename: 'Kanji'
            })
          }
        })
      }

    render() {
        const { classes, FETCH_QUERY, ADD_KANJI, updateKanjis } = this.props;
        const { columns, data } = this.state;
        if (FETCH_QUERY.loading){
            return <h1>Loading Dictionary....</h1>;
        }
        const kanjis = FETCH_QUERY.kanjis;
        this._subscribeToNewKanji(FETCH_QUERY.subscribeToMore)
        return (
            <div className='WelcomeContainer'>
                <div>
                    <Header title='Kanji Dictionary' />
                </div>
                <div className='Dictionary'>
                    <MaterialTable
                    title="Kanji Dictionary"
                    columns={columns}
                    data={kanjis}
                    editable={{
                        onRowAdd: newData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                            resolve();
                            ADD_KANJI({
                                variables: { character: newData.character, hiragana: newData.hiragana }
                            });
                            }, 200);
                        }),
                        onRowUpdate: (newData, oldData) =>
                        new Promise(resolve => {
                            setTimeout(() => {
                            resolve();
                            const data = [...data];
                            data[data.indexOf(oldData)] = newData;
                            this.setState({ ...this.state, data });
                            }, 600);
                        }),
                        onRowDelete: oldData =>
                        new Promise(resolve => {
                            setTimeout(() => {
                            resolve();
                            const data = [...data];
                            data.splice(data.indexOf(oldData), 1);
                            this.setState({ ...this.state, data });
                            }, 600);
                        }),
                    }}
                    />
                </div>
            </div>
        )
    }
}

Dictionary.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withRouter(
    compose(
        graphql(FETCH_QUERY, { name: 'FETCH_QUERY' }),
        graphql(ADD_KANJI, { name: 'ADD_KANJI' }),
        graphql(DELETE_KANJI, { name: 'UPDATE_KANJI' }),
        graphql(updateKanjis, { name: 'updateKanjis' }),
    )(withStyles(styles)(Dictionary)))