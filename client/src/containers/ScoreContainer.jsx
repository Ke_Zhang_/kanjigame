import React from 'react';
import PropTypes from 'prop-types';
import Buttons from '../components/Buttons';
import Header from '../components/Header';
import Counter from '../components/Counter';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../App.css';
import { withRouter } from 'react-router-dom'
import { Query } from 'react-apollo';
import { 
    gameResults
} from './Query';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display:'flex',
        flexDirection:'column',
        justifyContent:'space-around',
        alignItems:'center',
        height:300
    }, 
});  

const Score = ({classes}) => (
    <Query query={gameResults}>
      {({ data: { correctCount, totalTime } }) => (
        <div className='WelcomeContainer'>
            <div>
                <Header title='Please see the results of the game'/>
            </div>
            <div className='WelcomeButton'>   
                <Paper className={classes.root} elevation={1}>
                    <Typography variant="h5" component="h3">
                        Game Results are shown below
                    </Typography>
                    <Typography component="p">
                        Total Corrects
                    </Typography>
                    <Counter num={correctCount}/>
                    <Typography component="p">
                        Total Time in Seconds
                    </Typography>
                    <Typography gutterBottom variant="h4">
                        {totalTime}
                    </Typography>
                    <Buttons text='Replay' to='/' color='secondary' variant='contained'/>
                </Paper>  
            </div>
        </div> 
      )}
    </Query>
);

// class Score extends Component {
//     render() {
//         const { classes, correctCount, average} = this.props;
//         return (
//             <div className='WelcomeContainer'>
//                 <div>
//                     <Header title='Please see the results of the game'/>
//                 </div>
//                 <div className='WelcomeButton'>   
//                     <Paper className={classes.root} elevation={1}>
//                         <Typography variant="h5" component="h3">
//                             Game Results are shown below
//                         </Typography>
//                         <Typography component="p">
//                             Total Corrects
//                         </Typography>
//                         <Counter num={correctCount}/>
//                         <Typography component="p">
//                             Average time sec/kanji
//                         </Typography>
//                         <Typography gutterBottom variant="h4">
//                             {average}
//                         </Typography>
//                         <Buttons text='Replay' to='/' color='secondary' variant='contained'/>
//                     </Paper>  
//                 </div>
//             </div> 
//         ) 
//     }
// }

Score.propTypes = {
  classes: PropTypes.object.isRequired,
}   

export default withRouter((withStyles(styles)(Score)))