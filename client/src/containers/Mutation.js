import gql from 'graphql-tag';

const ADD_KANJI = gql`
mutation ADD_KANJI($character: String!, $hiragana: String!) {
    addkanji(character: $character, hiragana: $hiragana) {
        character
        hiragana
  }
}
`
const DELETE_KANJI = gql`
mutation DELETE_KANJI($id: ID!, $character: String!, $hiragana: String!) {
    deletekanji(id: $id, character: $character, hiragana: $hiragana) {
        id
        character
        hiragana
  }
}
`
const updateKanjis = gql`
mutation updateKanjis($kanjis: [Kanji!]!) {
    updateKanjis(kanjis: $kanjis) @client {
       kanjis
  }
}
`
const increaseCount = gql`
mutation increaseCount($count: Int!) {
  increaseCount(count: $count) @client {
      count
  }
}
`
const setTotalTime = gql`
mutation setTotalTime($totalTime: Int!) {
  setTotalTime(totalTime: $totalTime) @client {
      totalTime
 }
}
`
const setCorrectCount = gql`
mutation setCorrectCount($correctCount: Int!) {
    setCorrectCount(correctCount: $correctCount) @client {
        correctCount
 }
}
`
const resetToDefault = gql`
mutation resetToDefault($number: Int!) {
    resetToDefault(correctCount: $number, totalTime: $number, count: $number ) @client {
        correctCount,
        totalTime,
        count
 }
}
`

export {
    increaseCount,
    setTotalTime,
    setCorrectCount,
    resetToDefault,
    updateKanjis,
    ADD_KANJI,
    DELETE_KANJI
}