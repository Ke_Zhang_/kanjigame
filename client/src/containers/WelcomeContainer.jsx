import React, { Component } from 'react';
import PropTypes from 'prop-types';
import Buttons from '../components/Buttons';
import Header from '../components/Header';
import { withStyles } from '@material-ui/core/styles';
import Paper from '@material-ui/core/Paper';
import Typography from '@material-ui/core/Typography';
import '../App.css';
import { withRouter } from 'react-router-dom'
import { graphql, compose } from 'react-apollo';
import {
    resetToDefault
} from './Mutation';

const styles = theme => ({
    button: {
        margin: theme.spacing.unit,
    },
    input: {
        display: 'none',
    },
    root: {
        ...theme.mixins.gutters(),
        paddingTop: theme.spacing.unit * 2,
        paddingBottom: theme.spacing.unit * 2,
        display: 'flex',
        flexDirection: 'column',
        justifyContent: 'space-around',
        alignItems: 'center',
        height: 200
    },
});

class Welcome extends Component {

    componentDidMount() {
        const { resetToDefault } = this.props;
        resetToDefault({
            variables: { correctCount: 0, totalTime: 0, count: 0 }
        });
    }

    render() {
        const { classes } = this.props;
        return (
            <div className='WelcomeContainer'>
                <div>
                    <Header title='Welcome to Japanese Learning Game' />
                </div>
                <div className='WelcomeButton'>
                    <Paper className={classes.root} elevation={1}>
                        <Typography variant="h5" component="h3">
                            This is a Kanji Learning Game.
                        </Typography>
                        <Typography component="p">
                            You will need to answer 10 questions in the game, click button to start.
                        </Typography>
                        <Buttons text='Start' to='/learning' color='secondary' variant='contained'/>
                        <Buttons text='Dictionary' to='/dictionary' color='primary' variant='contained'/>
                    </Paper >
                </div>
            </div>
        )
    }
}

Welcome.propTypes = {
    classes: PropTypes.object.isRequired,
}

export default withRouter(
    compose(
        graphql(resetToDefault, { name: 'resetToDefault' })
    )(withStyles(styles)(Welcome)))