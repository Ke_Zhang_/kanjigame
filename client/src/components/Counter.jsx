import React from 'react';
import PropTypes from 'prop-types';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

const Counter = (props) => {
    const { num } = props;
    return (
        <div>
            <Grid container>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        {num}
                    </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        /
                        </Typography>
                </Grid>
                <Grid item>
                    <Typography gutterBottom variant="h4">
                        10
                        </Typography>
                </Grid>
            </Grid>
        </div>
    );
}

Counter.propTypes = {
    num: PropTypes.number.isRequired
};

export default Counter;