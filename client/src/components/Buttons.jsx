import React, { Component } from 'react';
import PropTypes from 'prop-types';
import { Link } from 'react-router-dom'
import Button from '@material-ui/core/Button';
import { withStyles } from '@material-ui/core/styles';

const styles = theme => ({
  button: {
    margin: theme.spacing.unit,
    width:200
  },
  input: {
    display: 'none',
  },
});

class Buttons extends Component {

    *renderButton() {
        const { classes, text, to, color, variant, action } = this.props;
        if(text === 'Skip' || text === 'Next'){
            yield (
                <Button variant={variant} color={color} className={classes.button} onClick={action} key={text}>
                    {text}
                </Button>
            );
        } else {
            yield (
                <Button component={Link} to={to} variant={variant} color={color} className={classes.button} key={text}>
                    {text}
                </Button>
            )
        }
    }

   render() {    
      return (
            <div>
                {[...this.renderButton()]}
            </div>
      );
   }
}

Buttons.propTypes = {
  classes: PropTypes.object.isRequired,
  text: PropTypes.string.isRequired,
  to: PropTypes.string,
  color: PropTypes.string.isRequired,
  variant: PropTypes.string.isRequired,
};

export default withStyles(styles)(Buttons);